from rocket import Rocket, aToS
import unittest
import logging
import sys

class rocketTest(unittest.TestCase):

	logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

	def testPhysics(self):

		rocket = Rocket(alt=0,dy=0, sysTime=0, verbose=False)
		expectedRocket = Rocket(alt=25,dy=10,senseAccY=2, sysTime=5, verbose=False)
		rocket.set('senseAccY',2)
		rocket.go(5)
		attributes = [a for a in dir(rocket) if not a.startswith('__') and not callable(getattr(rocket, a))]
		for i, val in enumerate(attributes):
			self.assertEqual(getattr(rocket, val), getattr(expectedRocket, val), 'Attr: {}'.format(val))

	def testDifferation(self):
		obj = OneDObj(0,0,3)
		expectedObject = OneDObj(13.5,9,3)

		for i in range(0,3):
			aToS(obj, 'displacement','velocity','acceleration')

		self.assertEqual(obj.displacement, expectedObject.displacement, "displacement")
		self.assertEqual(obj.velocity, expectedObject.velocity, "velocity")
		self.assertEqual(obj.acceleration, expectedObject.acceleration, "acceleration")

	def testFileParse(self):
		filename = "testJson.txt"
		f = open(filename,"w+")
		f.write("""{"Time Stamp":23594679,"System Time":6007,"System State":3,"Flight State":0,"Sense Acceleration X":-8.506423e26,"Sense Acceleration Y":0.751818,"Sense Acceleration Z":2.078273,"High Acceleration X":0.768,"High Acceleration Y":0.861,"High Acceleration Z":-0.645,"Gyroscope X":-0.058557,"Gyroscope Y":-1.14997,"Gyroscope Z":0.240354,"Magnetometer X":30.04284,"Magnetometer Y":-1.090786e36,"Magnetometer Z":21.29367,"Humidity":44.00687,"Temperature":33.49171,"Pressure":102150.9,"Altitude":-68.53448,"GPS Altitude":0,"GPS Latitude":0,"GPS Longitude":0,"GPS Fix Valid":true}\n""")
		f.write("""{"Time Stamp":23594679,"System Time":6030,"System State":3,"Flight State":0,"Sense Acceleration X":-8.506423e26,"Sense Acceleration Y":1.24026,"Sense Acceleration Z":0.876323,"High Acceleration X":-0.528,"High Acceleration Y":1.149,"High Acceleration Z":0.651,"Gyroscope X":-0.281467,"Gyroscope Y":-1.060753,"Gyroscope Z":0.273511,"Magnetometer X":30.04284,"Magnetometer Y":-1.090786e36,"Magnetometer Z":21.29367,"Humidity":44.0145,"Temperature":33.49171,"Pressure":102150.9,"Altitude":-68.53448,"GPS Altitude":0,"GPS Latitude":0,"GPS Longitude":0,"GPS Fix Valid":false}\n""")
		f.close()

		rocket = Rocket(verbose=False)
		expectedRocket = Rocket(timeStamp = 23594679, sysTime=6030, sysState=3, flightState=0, senseAccX=-8.506423e26, senseAccY=1.24026, senseAccZ=0.876323, highAccX=-0.528, highAccY=1.149, highAccZ=0.651, gyroX=-0.281467, gyroY=-1.060753, gyroZ=0.273511, magnetX=30.04284, magnetY=-1.090786e36, magnetZ=21.29367, humidity=44.0145, temp=33.49171, pressure=102150.9, alt=-68.53448, gpsAlt=0, gpsLat=0, gpsLong=0, gpsFixValid=False, verbose=False)
		rocket.simulate(filename)
		attributes = [a for a in dir(rocket) if not a.startswith('__') and not callable(getattr(rocket, a))]
		for i, val in enumerate(attributes):
			self.assertEqual(getattr(rocket, val), getattr(expectedRocket, val), 'Attr: {}'.format(val))



class OneDObj():
	def __init__(self,displacement, velocity, acceleration):
		self.displacement = displacement
		self.velocity = velocity
		self.acceleration = acceleration

