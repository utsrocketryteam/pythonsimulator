from rocket import Rocket, RocketInit, RocketStop

def main():
  try:
  	RocketInit()
  	rocket = Rocket()
  	rocket.simulate("Cylops_V2.TXT", True)

  finally:
  	RocketStop()
  	print("Press any key to exit")
  	input()
  
main()

