import json
from random import uniform
import time
import curses
import copy
from datetime import datetime

SIG_FIG = 5

def getTime():
    return time.mktime(datetime.now().timetuple())*1000

def aToS(obj, sAttr,vAttr, aAttr):
    s = getattr(obj,sAttr)
    v = getattr(obj,vAttr)
    a = getattr(obj,aAttr)

    setattr(obj,sAttr, (s + v +0.5*a))
    setattr(obj,vAttr, (v + a))

def RocketInit():
  cursesWin = curses.initscr()
  curses.start_color()
  curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK) # Green Text
  curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK) # Red Text
  curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK) #Yellow Text 
  curses.curs_set(0)
  return cursesWin

def getWin(verbose):
  if verbose:
    return RocketInit()
  else:
    return ""
def RocketStop():
  curses.curs_set(0)
  curses.endwin()
  print('\n\n\n\n\r') 

class Rocket():
  def __init__(self,
    displayIdx = 0,
    sysTime = getTime(),
    timeStamp = getTime(),
    gpsLat = round(-33.785500 + (uniform(-2,2)),SIG_FIG),
    gpsLong = round(151.116550+uniform(-2,2),SIG_FIG),
    alt = 60.11,
    gpsAlt = round(60.02 + uniform(-0.5,0.5),SIG_FIG),
    temp = round(23.22 + uniform(-3.5,2.5), SIG_FIG),
    humidity = round(0.01 + uniform(60,70),SIG_FIG),
    pressure = round(1041.55 + uniform(-13,15),SIG_FIG),
    sysState = 0,
    flightState = 0,
    senseAccX = round(0.01 + uniform(-10,10),SIG_FIG),
    senseAccY = round(0.01 + uniform(-10,10),SIG_FIG),
    senseAccZ = round(0.01 + uniform(-10,10),SIG_FIG),
    highAccX = round(0.01 + uniform(-10,10),SIG_FIG),
    highAccY = round(0.01 + uniform(-10,10),SIG_FIG),
    highAccZ = round(0.01 + uniform(-10,10),SIG_FIG),
    gyroX = round(0.01 + uniform(-10,10),SIG_FIG),
    gyroY = round(0.01 + uniform(-10,10),SIG_FIG),
    gyroZ = round(0.01 + uniform(-10,10),SIG_FIG),
    magnetX = round(0.01 + uniform(-10,10),SIG_FIG),
    magnetY = round(0.01 + uniform(-10,10),SIG_FIG),
    magnetZ = round(0.01 + uniform(-10,10),SIG_FIG),
    gpsFixValid = True,
    dx = 0,
    dy = 0,
    dz = 0,
    verbose = True
  ):
    self.__displayIdx__ = displayIdx
    self.__win__= getWin(verbose)
    self.sysTime = int(sysTime)
    self.timeStamp = int(timeStamp)
    self.gpsLat = float(gpsLat)
    self.gpsLong = float(gpsLong)
    self.alt = float(alt)
    self.gpsAlt = float(gpsAlt)
    self.temp = float(temp)
    self.humidity = float(humidity)
    self.pressure = float(pressure)
    self.sysState = int(sysState)
    self.flightState = int(flightState)
    self.senseAccX = float(senseAccX)
    self.senseAccY = float(senseAccY)
    self.senseAccZ = float(senseAccZ)
    self.highAccX = float(highAccX)
    self.highAccY = float(highAccY)
    self.highAccZ = float(highAccZ)
    self.gyroX = float(gyroX)
    self.gyroY = float(gyroY)
    self.gyroZ = float(gyroZ)
    self.magnetX = float(magnetX)
    self.magnetY = float(magnetY)
    self.magnetZ = float(magnetZ)
    self.gpsFixValid = gpsFixValid
    self._dx = float(dx)
    self._dy = float(dy)
    self._dz = float(dz)
    self.__verbose = verbose


  def __printLine(self, formattedText):
    self.__win__.addstr(self.__displayIdx__,0, formattedText)
    self.__displayIdx__ += 1
    self.__win__.refresh()

  def __printMCLine(self, formattedText1, formattedText2, colorPair1, colorPair2):
    self.__win__.addstr(self.__displayIdx__,0, formattedText1,curses.color_pair(colorPair1))
    self.__win__.addstr(self.__displayIdx__,len(formattedText1), formattedText2,curses.color_pair(colorPair2))
    self.__displayIdx__ += 1
    self.__win__.refresh()

  def log(self):
    self.__displayIdx__ = 0
    attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self, a))]
    for i, val in enumerate(attributes):
      self.__printLine('| {!s:>15} : {!s:>15} |'.format(val, getattr(self,val)))  

  def add(self, var, value):
    setattr(self,var, round(getattr(self,var) + value, SIG_FIG))
  
  def set(self, var, value):
    setattr(self, var, round(value, SIG_FIG))
  
  def json(self):
    
    return json.dumps({
      "Time Stamp": self.timeStamp,
      "System Time": self.sysTime,
      "System State": self.sysState,
      "Flight State": self.flightState,
      "Sense Acceleration X": self.senseAccX,
      "Sense Acceleration Y": self.senseAccY,
      "Sense Acceleration Z": self.senseAccZ,
      "High Acceleration X": self.highAccX,
      "High Acceleration Y": self.highAccY,
      "High Acceleration Z": self.highAccZ,
      "Gyroscope X": self.gyroX,
      "Gyroscope Y": self.gyroY,
      "Gyroscope Z": self.gyroZ,
      "Magnetometer X": self.magnetX,
      "Magnetometer Y": self.magnetY,
      "Magnetometer Z": self.magnetZ,
      "Humidity": self.humidity,
      "Temperature": self.temp,
      "Pressure": self.pressure,
      "Altitude": self.alt,
      "GPS Fix Valid": self.gpsFixValid,
      "GPS Altitude": self.gpsAlt,
      "GPS Latitude": self.gpsLat,
      "GPS Longitude": self.gpsLong,
    })

  def __logDeltas(self, initial):
    self.__displayIdx__ = 0
    attributes = [a for a in dir(self) if not a.startswith('_') and not callable(getattr(self, a))]
    for i, val in enumerate(attributes):
      value = getattr(self, val)
      initialValue = getattr(initial, val)

      if(type(value) in [float, int] and type(initialValue) in [float, int]):
        delta = round(value - initialValue, SIG_FIG)
        if(delta > 0):
          self.__printMCLine('| {!s:>16} : {!s:>16} |'.format(val, value), ' \u2191 {!s:>16} |'.format(delta), 0, 1)
        elif(delta < 0):
          self.__printMCLine('| {!s:>16} : {!s:>16} |'.format(val, value), ' \u2193 {!s:>16} |'.format(delta), 0, 2)
        else:
          self.__printLine('| {!s:>16} : {!s:>16} | - {!s:>16} |'.format(val, value, delta))
      elif(value != initialValue):
        self.__printMCLine('| {!s:>16} : {!s:>16} |'.format(val, value), ' {!s:>18} |'.format(value), 0, 3)
      else:
        self.__printLine('| {!s:>16} : {!s:>16} | - {:>16} |'.format(val, value, ""))

  def go(self, cycles=1):
    self.__displayIdx__ = 0
    initial = copy.copy(self)
    for i in range(0,cycles):
      self.add('sysTime', 1)
      time.sleep(1)
      if self.__verbose:
        self.__logDeltas(initial)
    if self.__verbose:
      self.__printLine('Completed {}/{} cycle(s)'.format(i+1, cycles))

  def go(self, cycles, *args: [str, any]):
    if self.__verbose:
      self.__win__.clear()
    initial = copy.copy(self)
    for i in range(0, cycles):
      for [var, val] in args:
        self.add(var, val)
      self.add('sysTime',1)
      aToS(self,'alt', '_dy','senseAccY')
      time.sleep(1)
      if self.__verbose:
        self.__logDeltas(initial)
        self.__printLine('Completed {}/{} cycle(s)'.format(i+1, cycles))
    
  def __fileParse(self, fileContents):
    content = json.loads(fileContents)
    self.sysTime = int(content["System Time"])
    self.timeStamp = int(content["Time Stamp"])
    self.gpsLat = float(content["GPS Latitude"])
    self.gpsLong = float(content["GPS Longitude"])
    self.alt = float(content["Altitude"])
    self.gpsAlt = float(content["GPS Altitude"])
    self.temp = float(content["Temperature"])
    self.humidity = float(content["Humidity"])
    self.pressure = float(content["Pressure"])
    self.sysState = float(content["System State"])
    self.flightState = float(content["Flight State"])
    self.senseAccX = float(content["Sense Acceleration X"])
    self.senseAccY = float(content["Sense Acceleration Y"])
    self.senseAccZ = float(content["Sense Acceleration Z"])
    self.highAccX = float(content["High Acceleration X"])
    self.highAccY = float(content["High Acceleration Y"])
    self.highAccZ = float(content["High Acceleration Z"])
    self.gyroX = float(content["Gyroscope X"])
    self.gyroY = float(content["Gyroscope Y"])
    self.gyroZ = float(content["Gyroscope Z"])
    self.magnetX = float(content["Magnetometer X"])
    self.magnetY = float(content["Magnetometer Y"])
    self.magnetZ = float(content["Magnetometer Z"])
    self.gpsFixValid = content["GPS Fix Valid"]


  def simulate(self, filename, verbose=False):
    if verbose:
      self.__win__.clear()
    file = open(filename)
    self.__fileParse(file.readline())
    file.close()
    initial = copy.copy(self)
    with open(filename) as file:
      for line in file:
        self.__fileParse(line)
        time.sleep(1)
        if verbose:
          self.__logDeltas(initial)
