# Python Simulator

The purpose of this application is to simulate the data coming in from the Arduino that is being fed to the Web Dashboard.
This would remove the need for an Arduino while testing changes to the Web Dashboard.

## Running the Simulator
This application is built on Python
No dependencies are required

There are two main ways of running the simulation:

- Using existing data
- Manually using the *go* function

### Using Existing Data
Existing recordings from prior flights can be used
**Cylops_V2.TXT** is included with the repo, but can be swapped out with any text file containing json in the correct structure & format

```python
from rocket import Rocket, RocketInit, RocketStop
	
try:
  RocketInit()
  rocket = Rocket()
  rocket.simulate("Cylops_V2.TXT", True) #True or false can be passed to run the simulation in verbose mode or not
	
finally: #Finally is used to ensure this will run even if the rocket misbehaves, preventing curses from ruining the terminal
  RocketStop()
```

### Manually using the Go function
Changes to the parameters being sent can be added.
This allows for custom events to happen, however it is more time consuming than relying on existing data
The rocket has additional parameters "dx,dy,dz" which are used to store velocity.
Changes to the senseAccY are converted to velocity (dy) then to alt in order to simulate basic motion.

```python
from rocket import Rocket, RocketInit, RocketStop

try:
  RocketInit()
  rocket = Rocket(alt=0, dy=0, sysTime=0)
  rocket.set('senseAccY',2)
  rocket.go(5) # go is used to start the simulation for a set number of cycles, it can also consume a list key value pairs to add to the rocket's parameters
  
finally:
  RocketStop()
```
### Setup Functions
```RocketInit()``` - initialises the screen

```RocketStop()``` - clears screen and stops curses

### Rocket Functions

```log()``` - prints the parameters held within the rocket

```add(variable_name, value)``` - adds value to the existing value of variable_name

```set(variable_name, value)``` - sets the value of variable_name to variable_name

```json()``` - returns a json representation of the rocket in its current state

```go(cycles)``` - runs the simulation for the number of cycles specified

```go(cycles, vararg [variable_name, value])``` - runs the simulation for the number of cycles specified, each cycle it will add value to its respective variable

```simulate(filename, verbose=False)``` - runs the simulation using the specified file, verbose mode prints to console using curses (default is false)


## To Do List

### Important
- ~~Rocket Object~~
- ~~User Interface~~
- ~~Basic Physics~~
- ~~Use existing data~~
- ~~Correct Json Output~~
- Pass over serial

### Low Priority
- Use internal verbose flag in the simulate function
- Timing
- Parsing .json files
- File Path parsing between OS's
- Folder Structure